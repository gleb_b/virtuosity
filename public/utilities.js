

/* SCROLLING */

var scrollChecker = true;
var scrollCount = 0;



function setChecker(state){
	scrollChecker = state;
}

function scrollCheck(Y) {
	
	if (scrollChecker == true) {



		setChecker(false);
		setTimeout(setChecker, 1000, true);
		scrollCount += Y;		
		if (scrollCount<0) {scrollCount = 0};		
		if (scrollCount>8) {scrollCount = 8}; 
		scrrollTo(scrollCount);



	}
}

window.addEventListener("wheel", event => 
	{
   		if (event.deltaY > 50) {scrollCheck(1);}
   		if (event.deltaY < -50) {scrollCheck(-1);}
   		
	}
);
 

/* Mobile Scroll */

var touchPos;

document.body.ontouchmove = function(e){
    let newTouchPos = e.changedTouches[0].clientY;
    let touchY = newTouchPos - touchPos;

   
    	if(newTouchPos > touchPos) {
        	scrollCheck(-1); 
    	}
    	if(newTouchPos < touchPos) {
        	scrollCheck(1); 
    }
}

document.body.ontouchstart = function(e){
    touchPos = e.changedTouches[0].clientY;
}


function scrrollTo(section) {


	document.body.setAttribute("scroll",section);
	

	/* Article activation */

	document.body.setAttribute("scroll",section);
	document.body.classList.remove("home", "strtgs", "thms", "contact");
	document.getElementById('bullets').setAttribute("scroll",section);
	document.getElementById('nav').classList.remove("active");
	Array.from(document.querySelectorAll('.navlink')).forEach(function(el) { el.classList.remove('active'); });
	Array.from(document.querySelectorAll('.bullet')).forEach(function(el) { el.classList.remove('active'); });
	Array.from(document.querySelectorAll('.strategy')).forEach(function(el) { el.classList.remove('active'); });
	Array.from(document.querySelectorAll('.theme')).forEach(function(el) { el.classList.remove('active'); });
	Array.from(document.querySelectorAll('.page')).forEach(function(el) { el.classList.remove('active'); });
	
	

	if (section == 0) {

		document.body.classList.add("home");
		document.getElementById(0).classList.add("active");
		document.getElementById(0).scrollIntoView({ behavior: "smooth"});
		
	}
	if (0 < section && section < 4) {

		document.body.classList.add("strtgs");
		document.getElementById('strategiesLink').classList.add("active");
		document.getElementById('strategy'+section).classList.add("active");
		document.getElementById('bullet'+section).classList.add("active");
		document.getElementById(1).scrollIntoView({ behavior: "smooth"});
		document.getElementById(1).classList.add("active");
			
	}
	if (3 < section && section < 8) {

		document.body.classList.add("thms");
		document.getElementById('themesLink').classList.add("active");
		document.getElementById('theme'+section).classList.add("active");
		document.getElementById('bullet'+section).classList.add("active");
		document.getElementById(4).scrollIntoView({ behavior: "smooth"});
		document.getElementById(4).classList.add("active");

	 
 

	}

	if (section == 8) {

		document.body.classList.add("contact");
		document.getElementById(8).classList.add("active");
		document.getElementById('contactsLink').classList.add("active");
		document.getElementById(8).scrollIntoView({ behavior: "smooth"});
		
	}
	
}


function navigate(order) {
	
	// console.info(order); 
	scrollCount = order;
	// console.info(scrollCount);
	scrrollTo(order);
}

function toggleMenu() { 
	 	document.getElementById('nav').classList.toggle("active"); 
 
}





// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
  e.preventDefault();
}

function preventDefaultForScrollKeys(e) {
  if (keys[e.keyCode]) {
    preventDefault(e);
    return false;
  }
}

// modern Chrome requires { passive: false } when adding event
var supportsPassive = false;
try {
  window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
    get: function () { supportsPassive = true; } 
  }));
} catch(e) {}

var wheelOpt = supportsPassive ? { passive: false } : false;
var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';

// call this to Disable
function disableScroll() {
  window.addEventListener('DOMMouseScroll', preventDefault, false); // older FF
  window.addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop
  window.addEventListener('touchmove', preventDefault, wheelOpt); // mobile
  window.addEventListener('keydown', preventDefaultForScrollKeys, false);
}

// call this to Enable
function enableScroll() {
  window.removeEventListener('DOMMouseScroll', preventDefault, false);
  window.removeEventListener(wheelEvent, preventDefault, wheelOpt); 
  window.removeEventListener('touchmove', preventDefault, wheelOpt);
  window.removeEventListener('keydown', preventDefaultForScrollKeys, false);
}


 disableScroll();

 // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

window.addEventListener('resize', function() {
		 vh = window.innerHeight * 0.01;
		// Then we set the value in the --vh custom property to the root of the document
		document.documentElement.style.setProperty('--vh', `${vh}px`);

}, true);






   function validateEmail( ) {
  		var re = /\S+@\S+\.\S+/;
  		return re.test(document.getElementById("emailInput").value);
	}

	function validateName() {
		if (document.getElementById("nameInput").value != ""){return true; }
	}

	function validateMessage() {
		if (document.getElementById("messageInput").value != ""){return true; }
	}

	function validateForm() {
		document.getElementById("messageSubmit").classList.remove("active");
		 if (validateName() && validateMessage() && validateEmail()) {
    	document.getElementById("messageSubmit").classList.add("active");
    	}
	}


	document.getElementById("emailInput").addEventListener('input', validateForm);
	document.getElementById("nameInput").addEventListener('input', validateForm);
	document.getElementById("messageInput").addEventListener('input', validateForm);


function confirmSending() {
	document.getElementById("messageSent").classList.add("active");
	document.getElementById("contact-form").reset();

	function hideConfirmation() {
		document.getElementById("messageSent").classList.remove("active");
	}

	setTimeout(hideConfirmation, 2000, true); 
}

        (function() {
            // https://dashboard.emailjs.com/admin/account
            emailjs.init('QKH32-wDe_lvKrMVq');
        })();

         function submitForm() {
            
                event.preventDefault();
                document.getElementById("messageSubmit").classList.remove("active");
                // generate a five digit number for the contact_number variable
                document.getElementById("numberInput").value = Math.random() * 100000 | 0;
                // these IDs from the previous steps
                emailjs.sendForm('service_virtuosity', 'template_oxm1pt8', document.getElementById("contact-form"))
                    .then(function() {
                        confirmSending();
                    }, function(error) {
                        console.log('FAILED...', error);
                    });
            
        }
 




 


var images = [];
function preloadImages() {
    for (var i = 0; i < arguments.length; i++) {
        images[i] = new Image();
        images[i].src = preloadImages.arguments[i]; 
    }
}

 window.onload = preloadImages(
    "img/theme-4-1.jpg",
    "img/theme-4-2.jpg",
    "img/theme-4-3.jpg",
    "img/theme-5-1.jpg",
    "img/theme-5-2.jpg",
    "img/theme-5-3.jpg",
    "img/theme-6-1.jpg",
    "img/theme-6-2.jpg",
    "img/theme-6-3.jpg",
    "img/theme-7-1.jpg",
    "img/theme-7-2.jpg",
    "img/theme-7-3.jpg"
);